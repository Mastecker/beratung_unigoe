# „3D-Druck“

### ...ein Verfahren mit vielfältigen Möglichkeiten der Gestaltung.
Mai 2017



### Was ist ein 3D-Druck?
3D-Druck ist ein Verfahren, bei dem dreidimensionale Objekte aufgebaut werden, indem Material dünn aufgetragen und verfestigt wird. Bereits seit den 80er Jahren wird der 3D-Druck in der Automobil- und Flugzeugbranche eingesetzt, um passgenaue Bauteile herzustellen. Die Größe dieser 3D-Drucker hat jedoch besondere Ausmaße angenommen, sodass sie nur in großen Betrieben eingesetzt werden konnten. In den letzten Jahren wurden daneben immer mehr 3D-Drucker entwickelt, die es Menschen zu Hause ermöglicht, individuelle Bauteile herzustellen und die auch in der Forschung zum Einsatz kommen.
Um ein Objekt dreidimensional drucken zu können, benötigt man eine Datei (CAD-Datei), die ein detailliertes 3D-Modell des Objektes beinhaltet.

### Wie funktioniert ein 3D-Drucker?
Die meisten 3D-Drucker arbeiten mittels sogenannten Fused Deposition Modeling (FDM). Dabei lassen sich nur Materialien verarbeiten, die bei höheren Temperaturen weich und formbar werden. Dafür sind beispielsweise thermoplastische Kunststoffe wie ABS oder PLA geeignet, aber auch Schokolade und Modellierwachs. Der Druckkopf eines 3D-Druckers besteht im Kern aus einer heißen Düse (extruder), in die das feste Rohmaterial hineingepresst und durch die hohe Temperatur verflüssigt wird. Durch die Öffnung der Düse tritt das Material als dünner und weicher Faden wieder hinaus und baut das Objekt Schicht für Schicht auf. Die äußere Kontur wird als Linie, Flächen oder als Schraffuren gedruckt.
Die meisten 3D-Drucker arbeiten mit FDM, andere Hersteller sprechen auch von Fused Filament Fabrication (FFF), da das feste Material, wie bei einer Heißklebepistole, in die Düse gedrückt wird.

Bei den meisten Objekten wird zusätzlich ein Gitter (raft) als Fundament erstellt, so verzieht sich weniger und es haftet besser auf dem Drucktisch. Überhänge oder Vorsprünge können abgestützt werden. Dies erfolgt bei den gängigen FDM-Maschinen mit Gitterstrukturen, die dann hinterher manuell vorsichtig abgebrochen oder abgeschliffen werden müssen.

Ein Verfahren, das komplett ohne Stützmaterial auskommt, nennt sich Selective Laser Melting (SLM) oder Selective Laser Sintering (SLS). Dabei ist das Rohmaterial ein Pulver, welches entsprechend der Form des gewünschten Objekts punktgenau „gedruckt“ wird. Ein UV-Laser führt dazu, dass das Pulver miteinander verschmilzt und fest wird. Das verbleibende, nicht mit UV bestrahlte Pulver kann das Objekt stützen, danach abgesaugt und wiederverwendet werden.
Zudem gibt es noch Stereolithografie-Maschinen (SLA), die besonders schöne und glatte Oberflächen erzeugen können. Dabei wird das Objekt in einem Becken mit flüssigem Kunstharz erstellt. Mit Hilfe eines UV-Lasers wird Schicht für Schicht das Kunstharz beleuchtet und härtet dadurch aus. Nach dem Belichten und Aushärten wird eine neue Schicht aufgetragen. Das restliche, das Objekt umgebende, flüssige Kunstharz kann entfernt werden. 
Die Entwicklung geeigneter Druckverfahren wird stetig weiter ausgebaut. 

### Welche Potentiale stecken im 3D Druck?
Die Preise der 3D-Drucker fallen, werden dadurch auch für Privatleute und kleinere Unternehmen zugänglich und die Möglichkeiten werden immer ausgereifter und vielfältiger. Die Entwicklung des 3D-Drucks kann als Revolution der Produktionsverhältnisse angesehen werden, weil die Produktion dezentralisiert wird und international genutzt werden kann. Die Daten für den 3D-Druck sind weltweit verfügbar und ortsunabhängig, da sie digital erstellt und verbreitet werden können. Jeder kann nun Objekte nach seinen Vorstellungen kreieren.
Aufgrund der vielen Vorteile findet der 3D-Druck nicht nur in der Automobil- oder Flugzeugindustrie Anwendung, sondern auch in der Lebensmittelindustrie (z.B. für die Herstellung von veganen Lebensmitteln), in der Designproduktion (z.B. Schmuckobjekte, Accessoires, Schuhe, Kleider) und Prothesenherstellung. Aber auch Bastler und Unternehmen nutzen den 3D-Druck für den Modellbau, Hobbyhandwerker und Architekten für die Erstellung von technischen Modellen und Forschungseinrichtung nutzen den Einsatz neuer Materialien für die Erforschung an lebendem Gewebe (Tissue Engineering).


### Wie erstellt man 3D-Modelle?
Mittels einer 3D-Software kann ein 3D-Modell erstellt werden. Das 3D-Modell wiederum muss dann in ein passendes Dateiformat exportiert und gespeichert werden. Die 3D-Modelle können mithilfe sogenannter CAD-Programme (Computer-Aided-Design) erstellt werden. In Anbetracht der schnellen Entwicklung des 3D-Drucks gibt es einige verschiedene Softwareprogramme, mit denen sich 3D-Modelle erstellen lassen (Softwareliste: http://www.uni-goettingen.de/de/3d-druck+software/571049.html).
Zudem gibt es Firmen, denen Sie ihre eigenen Vorstellungen des Objektes zuschicken können, damit ein digitales 3D-Modell für den 3D-Drucker erstellt werden kann. Dies ist natürlich nicht kostenfrei, weil eine Dienstleistung erbracht wird, Fabberhouse, i.materialise und Sculpteo sind beispielsweise Firmen, die diese Leistung anbieten.
Des Weiteren gibt es Plattformen wie Thingiverse, auf denen Nutzer ihre eigenen Modelle hochladen und zur weiteren Verwendung bereitstellen.
Im Folgenden sind drei beispielhafte Einsatzgebiete und dafür geeignete Software-Programme aufgelistet:

**Konstruieren von 3D-Modellen:** Selbst erzeugte Objekte werden geformt oder gescannte Objekte verformt, z.B. Personendarstellungen (Porträts oder Figuren), Tiere oder Pflanzen, ungeometrische oder ungegenständliche Objekte. Für diesen Einsatz kann z.B. folgende Software verwendet werden: ZBrush, Sculptris, SculptGL, u.U. aber auch Blender, Meshmixer 

**Collagieren von 3D-Modellen:** Downloads oder vorgegebene Objekte werden miteinander kombiniert. Dabei können Bestandteile getrennt und anders zusammengefügt werden, Dimensionen verfremdet und fantastische Kombinationen eingegangen werden. Für diesen Einsatz kann z.B. folgende Software verwendet werden: Netfabb, Meshmixer, MeshLab, aber u.U. auch SketchUp, Tinkercad, ZBrush, Sculpris, SculptGL, Blender.
*Beachte: Es gibt keinen „Alleskönner“ unter der 3D-Software. Manche Software eignet sich zu mehreren Einsatzzwecken, teils wiederum nur eingeschränkt.*


### Wo kann ich 3D-Modelle drucken?
Man kann selbst ein 3D-Modell mit einer geeigneten Software erstellen, sich im Internet Dateien zu bereits vorhandenen 3D-Modellen herunterladen oder sich von geeigneten Firmen Modelle erstellen lassen. Wenn man die 3D-Modelle digital vorliegen hat, gibt es in **Göttingen** z.B. in der Bereichsbibliothek der Medizin im Klinikum die Möglichkeit sich ein Objekt ausdrucken zu lassen und/oder Beratung in der Erstellung eines eigenen 3D-Modells zu bekommen.

* Universität Göttingen – Bereichsbibliothek der Medizin (zwei 3D-Drucker: Makerbot Replicator 5)
    * Modelle ausdrucken, einscannen oder modellieren an den Uni-Rechnern oder Zuhause
  * Kosten entstehen lediglich beim Ausdruck des 3D-Modells: Es geht nach Gewicht (1 Gramm = 0,10€). 
* HAWK Göttingen:
  - Die HAWK probiert mit den Studierenden eigene Entwicklungen aus.
* Co-Working Space „Hafven“ in Hannover:
  * Der Space besitzt eine Werkstatt und bietet passende Einführungskurse an.
* Chaos Computer Club Standort Göttingen: 
  * https://cccgoe.de/wiki/3D-Drucker


### Wie kann 3D-Druck in der Schule oder Universität eingesetzt werden?

* Das X-LAB Göttingen bietet für Schulklassen in einer AG die Erstellung eines 3D-Drucks an:   http://www.xlab-goettingen.de/3d-druck.html
* Entwurf einer Unterrichtseinheit für die Schule: http://www.schulentwicklung.nrw.de/cms/upload/Faecher_Seiten/faecheruebergreifend/3D-Druck_in_der_Schule.pdf
* Zwei Beispiele für den Einsatz des 3D-Drucks in der universitären Lehre sind z.B. 
  * Erstellung von Proteinmodellen aus Daten der PDB für Biochemie-Kurse für ein erweitertes Molekülverständnis. 
  * An der HAWK gibt es das Akkuschrauber-Rennen mit Fahrzeugen aus dem 3D-Drucker im FB Produktdesign.


#### Beispiele für die Forschung

* Universität Siegen 
(entwickelt ein Stadtmodell mit dem 3D-Drucker) 
* Ruhr Universität Bochum
(Forschung zu einem „Bioprinter“, mit dem menschliche Zellen erstellt werden)
* University College Cork
(Testen den Druck von künstlichem Käse mit 3D-Druckern)

##### Quellen
* [http://www.spiegel.de/netzwelt/gadgets/3d-drucker-ein-fuehrung-fuer-einsteiger-a-977405.html](http://www.spiegel.de/netzwelt/gadgets/3d-drucker-ein-fuehrung-fuer-einsteiger-a-977405.html)
* [https://www.tintenmarkt.de/Presseberichte/Der-3D-Druck-was-ist-das-und-wie-funktioniert-er-aid-1665.html](https://www.tintenmarkt.de/Presseberichte/Der-3D-Druck-was-ist-das-und-wie-funktioniert-er-aid-1665.html)
* [http://www.schulentwicklung.nrw.de/cms/upload/Faecher_Seiten/faecheruebergreifend/3D-Druck_in_der_Schule.pdf](http://www.schulentwicklung.nrw.de/cms/upload/Faecher_Seiten/faecheruebergreifend/3D-Druck_in_der_Schule.pdf)
* [https://www.sub.uni-goettingen.de/kopieren-digitalisieren/self-service](https://www.sub.uni-goettingen.de/kopieren-digitalisieren/self-service)

